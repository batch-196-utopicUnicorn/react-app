import {useState, useEffect} from 'react';
import ItemCard from '../components/ItemCard'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';




export default function Product(){

	const [showing, setShowing] = useState(false);

  	const handleClosing = () => setShowing(false);
  	const handleShowing = () => setShowing(true);

	const [item, setItem] = useState([])
	const [title, setTitle] = useState('')
	const [type, setType] = useState('')
	console.log(title)
	let price = 0

	if(type == 'DVD'){
		price = 50
	}else{
		price = 25
	}

		useEffect(() =>{
		fetch('https://sheltered-ravine-70119.herokuapp.com/item/')
		.then(res => res.json())
		.then(data => {
			setItem(data.map(datas =>{
				console.log(data)
				return(
					<ItemCard key={datas._id} itemProp = {datas}/>
					)
			}))

		})
	}, [])

		function createProduct(e){
			fetch('https://sheltered-ravine-70119.herokuapp.com/item/new', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					title: title,
					price: price,
					type: type
				})
			})
			.then(result => result.json())
			.then(data => console.log(data))
		}

		function createOrder(){
		createProduct()
		window.location.reload(false)
	}




		return(
			<>
			<h1>Products</h1>
			<Button variant="primary" onClick={handleShowing}>
        Create new product
      </Button>
			{item}


      <Modal show={showing} onHide={handleClosing}>
        <Modal.Header closeButton>
          <Modal.Title>New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form >
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Title:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Video title"
                autoFocus
                onChange={e => setTitle(e.target.value)}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Video Type:</Form.Label>
              <Form.Select aria-label="Default select example" onChange={e => setType(e.target.value)}>
     		 
     		 <option value="DVD">DVD</option>
     		 <option value="VCD">VCD</option>
   			 </Form.Select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClosing}>
            Close
          </Button>
          <Button variant="primary" onClick={e => createOrder(e)}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>
			</>
		)

}
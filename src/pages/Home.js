import CustomerCard from '../components/CustomerCard'
import react, {useState, useEffect} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-router-dom'

export default function Users(){

	const [show, setShow] = useState(false);

 	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

	const [fullName, setFullName] = useState('')
	const [customer, setCustomer] = useState([]);


	function create(e){
		
		fetch('https://sheltered-ravine-70119.herokuapp.com/customer/new',{
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				fullName: fullName
			})
		})
		.then(res => res.json())
		.then(data => console.log(data))
		alert('New Customer added')
	}

	useEffect(() => {
		fetch('https://sheltered-ravine-70119.herokuapp.com/customer/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCustomer(data.map(cust =>{
				return(
					<CustomerCard key={cust._id} customerProp = {cust}/>
					)
			}))
		})
	}, [])
	

	return(
		  <>
		  <h1 className="text-center">Bogsy Video Store</h1>
		  <Container>
		  <Row>
      <Button className="my-3" variant="primary" onClick={handleShow}>
        Create new Customer
      </Button>
      <Button className="mb-3" variant="primary" as={Link} to="/product">
        View Products
      </Button>
      </Row>
      </Container>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Customer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={e => create(e)}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Full Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Full Name"
                autoFocus
                onChange={e => setFullName(e.target.value)}
              />
            </Form.Group>
            <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" type="submit">
            Create
          </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          
        </Modal.Footer>
      </Modal>

      {customer}

    </>
	)
}
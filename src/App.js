import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap';
import './App.css';
import Home from './pages/Home'
import Product from './pages/Product'


export default function App() {
  return (
    <>
    <Router>
    <Container>
    <Routes>
    <Route exact path='/' element={<Home/>}/>
    <Route exact path='/product' element={<Product/>}/>
    </Routes>
    </Container>
    </Router>
    </>
  )
}


import {Card, Button} from 'react-bootstrap'
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import react, {useState, useEffect} from 'react';
import RentCard from './RentCard'
import ItemCard from './ItemCard'


export default function CustomerCard(props){
	
	const [newFullName, setNewFullName] = useState('')
	const [rentItem, setRentItem] = useState([])
	console.log(rentItem)

	const [item, setItem] = useState([])
	console.log(item)


	const [title, setTitle] = useState('')
	//const [itemId, setItemId] = useState('')
	const [price, setPrice] = useState('')
	const [type, setType] = useState('')
	console.log(title)
	console.log(price)
	console.log(type)


	const [show, setShow] = useState(false);
 	const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [shows, setShows] = useState(false);
 	const handClose = () => setShows(false);
  const handShow = () => setShows(true);

	const {fullName, _id} = props.customerProp;
	

	useEffect(() => {
		fetch(`https://sheltered-ravine-70119.herokuapp.com/customer/viewRent/${_id}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setRentItem(data.map(itemm =>{
				return(
					<RentCard key={itemm._id} rentProp = {itemm}/>
					)
			}))
		})
	}, [])

	useEffect(() =>{
		fetch('https://sheltered-ravine-70119.herokuapp.com/item/')
		.then(res => res.json())
		.then(data => {
			setItem(data.map(datas =>{
				return(
					<ItemCard key={datas._id} itemProp = {datas}/>
					)
			}))

		})
	}, [])


	function order(e){
		fetch(`https://sheltered-ravine-70119.herokuapp.com/customer/rent/${_id}`,{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: title,
				itemId: 'aasdqwe',
				price: price,
				type: type
			})
		})
		.then(res => res.json())
		.then(data => console.log(data))
	}


	



	function edit(e){
		fetch(`https://sheltered-ravine-70119.herokuapp.com/customer/edit/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				fullName: newFullName
			})
		})
		.then(res => res.json())
		.then(data => console.log(data))
	}


	function delet(e){
		fetch(`https://sheltered-ravine-70119.herokuapp.com/customer/delete/${_id}`,{
			method: "DELETE",
			headers: {
				'Content-Type' : 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => console.log(data))
	}

	function refreshOnUpdate(){
		edit()
		window.location.reload(false)
	}

		function orderOnUpdate(){
		order()
		window.location.reload(false)
	}



	return(
		<>
		<Card style={{ width: '100%' }} className="p-3 mb-3">
      
      <Card.Body>
        
        <Form onSubmit={e => delet(e)}>
        <Card.Title className="mb-3">{fullName}</Card.Title>
        <Card.Subtitle className="mb-3">Rented Items:</Card.Subtitle>
        <ul>{rentItem}</ul>
        
        
        <Button variant="primary" onClick={handleShow}>
        Edit
      </Button>

      <Button variant="primary" onClick={handShow}>
        Rent
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Customer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Full Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Full Name"
                autoFocus
                onChange={e => setNewFullName(e.target.value)}
              />
            </Form.Group>
            <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={e => refreshOnUpdate(e)}>
            Save
          </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          
        </Modal.Footer>
      </Modal>

      <Modal show={shows} onHide={handClose}>
        <Modal.Header closeButton>
          <Modal.Title>Rent</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Title</Form.Label>
              <Form.Select aria-label="Default select example" onChange={e => setTitle(e.target.value)}>
      				<option>Select Video</option>
     					<option value="Madagascar">Madagascar</option>
      				<option value="Lion Tiger Gate">Lion Tiger Gate</option>
     				 <option value="Frozen 2">Frozen 2</option>
    			</Form.Select>
            </Form.Group>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Type</Form.Label>
              <Form.Select aria-label="Default select example" onChange={e => setType(e.target.value)} required>
      				<option >Please Select type</option>
      				<option value="VCD">VCD</option>
     					<option value="DVD">DVD</option>
     				 
    			</Form.Select>

    			{
    				(type === 'DVD')?
    				<Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Select aria-label="Default select example" onChange={e => setPrice(e.target.value)} required>
      				<option >Select Price</option>
     					<option value="50">50</option>
     				 
    			</Form.Select>
            </Form.Group>
            :
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Select aria-label="Default select example" onChange={e => setPrice(e.target.value)} required>
      				<option >Select Price</option>
     					<option value="25">25</option>
     				 
    			</Form.Select>
            </Form.Group>
    			}

            </Form.Group>
            <Button variant="secondary" onClick={handClose}>
            Close
          </Button>
          <Button variant="primary" onClick={e => orderOnUpdate(e)}>
            Submit
          </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          
        </Modal.Footer>
      </Modal>



        <Button className="btn-danger" type="submit">Delete</Button>
        </Form>
      </Card.Body>
    </Card>
    
    
    </>
	)
}
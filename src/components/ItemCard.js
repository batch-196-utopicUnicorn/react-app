import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import react, {useState} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';



export default function ItemCard(props){

	console.log(props)
	const [show, setShow] = useState(false);

  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);
	const [shows, setShows] = useState(false);

	const {title, price, type, _id} = props.itemProp
	console.log(_id)

	
	const [titlee, setTitlee] = useState('')
	const [typee, setTypee] = useState('')
	console.log(title)
	let pricee = 0

	if(type == 'DVD'){
		pricee = 50
	}else{
		pricee = 25
	}

	function deleteProduct(){
		fetch(`https://sheltered-ravine-70119.herokuapp.com/item/delete/${_id}`,{
			method: "DELETE",
			headers: {
				'Content-Type' : 'application/json'
			}
		})
		.then(res => res.json())

	}

	function deleteAndUpdate(){
		deleteProduct()
		window.location.reload(false)
	}

	function updateProduct(){
		fetch(`https://sheltered-ravine-70119.herokuapp.com/item/edit/${_id}`, {
			method: "PUT",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
					title: titlee,
					price: pricee,
					type: typee
			})
		})
		.then(res => res.json())
		.then(data => console.log(data))
	}

	function updateAndUpdate(){
		updateProduct()
		window.location.reload(false)
	}

	return(
		<>

		<Card className="m-5" style={{ width: '22rem' }}>
      	<ListGroup variant="flush">
        <ListGroup.Item>Title: {title}</ListGroup.Item>
        <ListGroup.Item>Price: ₱{price}</ListGroup.Item>
        <ListGroup.Item>Type: {type}</ListGroup.Item>
      	</ListGroup>
		<Button className="btn-primary" onClick={handleShow}>Update</Button>
      	<Button className="btn-danger" onClick={() => deleteAndUpdate()}>Delete</Button>
   		</Card>


   		<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form >
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Title:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Video title"
                autoFocus
                onChange={e => setTitlee(e.target.value)}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Video Type:</Form.Label>
              <Form.Select aria-label="Default select example" onChange={e => setTypee(e.target.value)}>
     		 
     		 <option value="DVD">DVD</option>
     		 <option value="VCD">VCD</option>
   			 </Form.Select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={e => updateAndUpdate(e)}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>

		</>
	)
}